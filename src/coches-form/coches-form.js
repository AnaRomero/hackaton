import {LitElement, html} from 'lit-element';
import '../coches-busqueda/coches-busqueda.js'
import '../coches-busqueda-reserva/coches-busqueda-reserva.js'


class CochesForm extends LitElement {

    static get properties(){
        return{
            filtrosBusqueda:{type: Array},
            datosReserva:{type:Object},
            coches:{type:Array},
            buildings: {type: Array},
            showBookingForm: {type: Boolean},
            filtrosBusqueda:{type: Array},
            selectionCar:{type: Array},
            persona:{type: String},

        }    
    }

    constructor(){
        super();

        this.buildings = [];
        this.showBookingForm = false;
        this.filtrosBusqueda = [];
        this.filtrosBusqueda.muestraReservas = "off";
        this.listBuildings();
        this.datosReserva = [];

        
    }



    render(){
        return html`
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <div class="col row ml-2 bg-light" >
        <form>
            <div class="col container card border-light form-group">
                <h5 class="card-header">Busca los vehículos disponibles</h5>
                </br>
                </br>
                <div class="col-md-9 mb-3">
                    <label>Sede Origen</label>
                    <select class="form-control" id="sOrigBuilding" @change="${this.updateOriginBuild}">
                        <option selected disabled>Selecciona...</option>
                    </select>
                </div>    

                </br>
            
                <div class="col-md-9 mb-4">
                    <label>Fecha de desplazamiento</label>
                    <input type="date" class="form-control col-md-10" id="ffecha" @input="${this.updateDate}"></input>
                </div>
            
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="OcultaReservado" @click="${this.filtroReservados}"/>
                    <label class="custom-control-label" for="OcultaReservado">Mostrar vehículos no disponibles</label>
                </div>
                </br></br>
            </div>  
        </form>   

        <form class="d-none" id="fReserva">
            <div class="container card border-light form-group" >
                <h5 class="card-header">Confirma la reserva de tu vehículo</h5>
                </br>
                <div class="col-md-9 mb-4">
                <label>Vehículo seleccionado</label>
                <input type="text" class="form-control" disabled 
                .value="${this.datosReserva.vehiculos}"/>
                </div>
                <div class="col-md-9 mb-4">
                <label>Titular de la reserva</label>
                <input type="text" id="fPersona" class="form-control" @input="${this.updatePerson}"
                />
                </div>
                <div class="col-md-9 mb-4 col-4">
                    <label>Sede Destino</label>
                    <select class="form-control" id="sDestBuilding" @change="${this.updateDestBuild}" disabled>
                    <option selected disabled value="">Selecciona...</option>
                    </select>
                </div>    
                </br>
            </div>  
        </form> 
        </div>    
        <div class="col-md-9 mb-3">
            <button class="btn-primary btn-lg" id="bListarCoches" @click="${this.searchCars}">Buscar</button>
            <button class="btn-success btn-lg d-none" id="bReservar" @click="${this.requestBooking}">Reservar</button>
        </div> 
        <button class="btn btn-primary sm-1" @click="${this.selectCar}">Coche seleccionado</button> 

       
              
        `;
    }

    updated(changeProperties){
        console.log("[updated]");
        if(changeProperties.has("buildings")){
            this.updateListBuildings("sOrigBuilding");
            this.updateListBuildings("sDestBuilding");
            console.log("Actualizado ambos select")

            
        }
    }

    //Actualizo si el usuario marca el check de filtrado. por defecto es false.
    filtroReservados(e){
        console.log("Selecciono Mostrar Vehículos Resevados " + e.target.value);
        this.filtrosBusqueda.muestraReservas = e.target.value;
    }

     //Recoger Origen seleccionado por usuario
     updateOriginBuild(e){
        console.log("Se ha seleccionado una sede de origen: " + e.target.value);
        this.filtrosBusqueda.sedeOrigen = e.target.value;
        
    }

    //Recoger fecha insertada por usuario
    updateDate(e){
        console.log("Se ha insertado una fecha: " + e.target.value);
        this.filtrosBusqueda.fechaDespl = e.target.value;
       
    }

    //Cargar elemento (select) de listado de edificios de origen y destino
    updateListBuildings(idListado){
        console.log("Cargamos listado");
        let select =  this.shadowRoot.getElementById(idListado);
      
        //Recorremos el array.
        this.buildings.forEach(build => {
        console.log(build.name);
            var opt = document.createElement('option');
            opt.name = build.name;
            opt.id = build.id;
            opt.innerHTML = build.name;
            select.appendChild(opt);
        });

    }

    //Solicitar reserva
    requestBooking(e){
        e.preventDefault();
        console.log("Se ha pulsado el botón de rserva con los siguientes datos:");
        //Incluir llamada al servicio de reserva
        console.log(this.datosReserva);
        let body = JSON.stringify(this.datosReserva);
        console.log(body);
        this.conexionAPIPOSTReserva(body);


    }


     //Realizar búsqueda según los filtros añadidos
     searchCars(e){
        e.preventDefault();
        console.log("Se ha pulsado Buscar con los siguietnes datos");
    
        console.log(this.filtrosBusqueda);
        
        this.conexionAPIGETvehiculos();

        //salida mock
        this.coches = [{"id":"0","matricula":"0000 LDP","sede":"Sede 0","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"1","matricula":"0001 LDP","sede":"Sede 1","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"2","matricula":"0002 LDP","sede":"Sede 2","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"3","matricula":"0003 LDP","sede":"Sede 3","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"4","matricula":"0004 LDP","sede":"Sede 4","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"5","matricula":"0005 LDP","sede":"Sede 5","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"6","matricula":"0006 LDP","sede":"Sede 6","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"7","matricula":"0007 LDP","sede":"Sede 7","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"8","matricula":"0008 LDP","sede":"Sede 8","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0},{"id":"9","matricula":"0009 LDP","sede":"Sede 9","marca":"Toyota","modelo":"Yaris","fechas":[],"carga":1.0}];
        console.log("Salida mock del GET vehiculos");
        console.log(this.coches);

    }

    //BORRAR - Método que emula la selecicón de un coche - BORRAR
    selectCar(e){
        console.log("Se ha seleccionado un coche para reservar");
        //Cargar coche-busqeuda-reserva con los datos que he seleccionado
        let selecCarMock ={
            matricula:"1234-XXX",
            marca: "Ford",
             modelo: "Cmax"
        };
        
        this.datosReserva.id = "0001";
        this.datosReserva.vehiculos = selecCarMock.marca;
        this.datosReserva.matricula = selecCarMock.matricula;
        this.datosReserva.fecha = this.filtrosBusqueda.fechaDespl;
        this.datosReserva.origen = this.filtrosBusqueda.sedeOrigen;
        
        this.shadowRoot.getElementById("fReserva").classList.remove("d-none");
        
        
    }

    //Recoger nombre de la persona introducido por usuario
    updatePerson(e){
        console.log("Se ha introducido el nombre de la persona: " + e.target.value);
       
       this.datosReserva.nombre = e.target.value;
       this.shadowRoot.getElementById("sDestBuilding").disabled = false;
    }


    //Recoger edificio de destino solicitado por usuario
    updateDestBuild(e){
        
        console.log("Se ha seleccionado una sede de destino: " + e.target.value);
        this.datosReserva.destino = e.target.value;
        this.shadowRoot.getElementById("bReservar").classList.remove("d-none");
        
    }

    //Obtener el listado de edificios
    listBuildings(){
       this.conexionAPIGETsedes();
       
       //MOCK
        console.log("[listBuildings] Inicializamos el mock de edificios");
        this.buildings = [{"id":"001","name":"Sede 001"},{"id":"002","name":"Sede 002"},{"id":"003","name":"Sede 003"},{"id":"004","name":"Sede 004"},{"id":"005","name":"Sede 005"}]
        console.log("[listBuildings] Se ha cargado la lista de edificios: ");
        console.log(this.buildings);
    }

    

    conexionAPIGETvehiculos(){
        //Llamada a servicio de búsqueda
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("[get vehículos] Petición completada correctamente");

                console.log("[get vehiculos] "+ xhr.responseText);

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                
                this.coches = APIResponse.results;
                
                console.log("Get vehiculos obtenido: " + this.coches);

            }
        };

                xhr.open("GET", "http://localhost:8080/hackathon/v1/vehiculos");

                xhr.send();
                console.log("[get] Fin de get");
    }

    conexionAPIGETsedes(){
                let xhr = new XMLHttpRequest();

                xhr.onload = () => {
                    if(xhr.status === 200){
                        console.log("[get sedes] Petición completada correctamente");
                        console.log("[get sedes] "+ xhr.responseText);
        
                        let APIResponse = JSON.parse(xhr.responseText);
                        console.log(APIResponse);
                        
                        this.buildings = APIResponse.results;
                        
                        console.log("Get sedes obtenido: " + this.buildings);
        
                    }
                };
        
                        xhr.open("GET", "http://localhost:8080/hackathon/v1/sedes");
                       
                        xhr.send();
                        console.log("[get] Fin de get");
    }

    conexionAPIPOSTReserva(body){
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("[post reserva] Petición completada correctamente");
                console.log("[post reserva] "+ xhr.responseText);

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                
                //this.buildings = APIResponse.results;
                
                console.log("post reserva obtenido: " + this.buildings);

            }
        };

                xhr.open("GET", "http://localhost:8080/hackathon/v1/reservas");
                //Se envía body
                xhr.send(body);
                console.log("[get] Fin de get");
}
    

  

    

}

customElements.define('coches-form', CochesForm);