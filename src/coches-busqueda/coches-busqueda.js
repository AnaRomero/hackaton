import { LitElement, html } from 'lit-element';

class CochesBusqueda extends LitElement {

    static get properties(){
        return{
            buildings: {type: Array},
            showBookingForm: {type: Boolean},
            filtrosBusqueda:{type: Array}

        }    
    }

    constructor(){
        super();

        this.buildings = [];
        this.showBookingForm = false;
        this.filtrosBusqueda = [];

        //Inicializamos los datos de sedes (pte integrar Servicios)
        this.listBuildings();
    }

    
    updated(changeProperties){

        console.log("[updated]");
        if(changeProperties.has("buildings")){

            this.updateListBuildings();
            
        }


    }
    
    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
       <form>
            <div class="col container card form-group">
                <h5 class="card-header">Busca los vehículos disponibles</h5>
                </br>
                </br>
                <div class="col-md-9 mb-3">
                    <label>Sede Origen</label>
                    <select class="form-control" id="sOrigBuilding" @change="${this.updateOriginBuild}">
                        <option selected disabled>Selecciona...</option>
                    </select>
                </div>    

                </br>
            
                <div class="col-md-9 mb-4">
                    <label>Fecha de desplazamiento</label>
                    <input type="date" class="form-control col-md-10" id="ffecha" @input="${this.updateDate}"></input>
                </div>
            
                </br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="OcultaReservado"/>
                    <label class="custom-control-label" for="OcultaReservado">No mostrar vehículos reservados</label>
                </div>
                </br></br>
            </div>  
        </form>    
        `;
    }
     

    
   
}

customElements.define('coches-busqueda', CochesBusqueda);