import {LitElement, html} from 'lit-element';

import '../coches-header/coches-header.js';
import '../coches-form/coches-form.js';
import '../coches-listado/coches-listado.js';

class CochesApp extends LitElement {
    render(){
        return html`
            <div>Hola App</div>
            <coches-header></coches-header>
            <coches-form></coches-form>
            <coches-listado></coches-listado>
        `;
    }
}

customElements.define('coches-app', CochesApp);