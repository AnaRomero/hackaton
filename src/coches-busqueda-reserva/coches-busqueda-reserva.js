import { LitElement, html } from 'lit-element';

class CochesBusquedaReserva extends LitElement {

    static get properties(){
        return{
            buildings: {type: Array},
            selectionCar:{type: Array},
            persona:{type: String},
            destBuild:{type:Array},
            persona:{type:String},
            sedeDestino:{type:String}
            
        }    
    }


    updated(changeProperties){
        
        if(changeProperties.has("buildings")){
            console.log("Sew ha modifciado edificios")
            this.updateListBuildings();
        }
    }

    constructor(){
        super();

        this.buildings = [];
        this.selectionCar = [];
        this.persona = "";

        //Inicializamos los datos de sedes (pte integrar Servicios)
        this.listBuildings();
    }
    
    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <form>
             <div class="container card form-group" >
                 <h5 class="card-header">Confirma la reserva de tu vehículo</h5>
                 </br>
                 <div class="col-md-9 mb-4">
                 <label>Vehículo seleccionado</label>
                 <input type="text" class="form-control" disabled 
                 .value="${this.selectionCar.marca + " " + this.selectionCar.modelo + " | " + this.selectionCar.matricula}"/>
                 </div>
                 <div class="col-md-9 mb-4">
                    <label>Titular de la reserva</label>
                    <input type="text" id="fPersona" class="form-control" @input="${this.updatePerson}"
                    />
                 </div>

                 <div class="col-md-9 mb-4 col-4">
                     <label>Sede Destino</label>
                     <select class="form-control" id="sDestBuilding" @change="${this.updateDestBuild}">
                        <option selected disabled value="">Selecciona...</option>
                     </select>
                 </div>    
             

             
                 <div class="col-md-9 mb-4">
                     
                 </div>

                 </br>
             </div>  
         </form>      
        
        `;
    }




    //Nombre de la persona introducido
    updatePerson(e){
        console.log("Se ha introducido el nombre de la persona: " + e.target.value);
       // this.shadowRoot.getElementById("sDestBuilding").disabled = false;
       this.persona = e.target.value;
    }


    //Edificio destino seleccionado
    updateDestBuild(e){
        
        console.log("Se ha seleccionado una sede de destino: " + e.target.value);
        this.destBuild = e.target.value;
        this.dispatchEvent(new CustomEvent("habilita-reserva",{
            detail:{
                datosReserva:{
                    sedeDestino: e.target.value,
                    persona: this.persona,
                    matricula:this.selectionCar.matricula,
                    modelo: this.selectionCar.modelo,
                    marca:this.selectionCar.marca
                }
            }
        }));
        
    }

    //Obtener el listado de edificios
    listBuildings(){
        console.log("[listBuildings] Inicializamos el mock de edificios");
        this.buildings=[
            {name: "Tablas I"},{name: "Tablas II"},{name: "Tablas III"},{name: "Ciudad BBVA"},{name: "Tres Cantos"}
        ];
        console.log("[listBuildings] Se ha cargado la lista de edificios: ");
        console.log(this.buildings);
    }

    //Cargar elemento (select) de listado de edificios de origen y destino
    updateListBuildings(){

        let select =  this.shadowRoot.getElementById("sDestBuilding");
          
        //Recorremos el array.
        this.buildings.forEach(build => {
        console.log(build.name);
            var opt = document.createElement('option');
            opt.name = build.name;
            opt.innerHTML = build.name;
            select.appendChild(opt);
        });

    }


}

customElements.define('coches-busqueda-reserva', CochesBusquedaReserva);